# Semáforo Angular
## Enunciado
1. Crea un nuevo proyecto de Angular con ayuda de angular CLI. Haz que el prefijo que use la aplicación sea "semaforo".
2. Copia el código CSS de esta página al nuevo proyecto, donde corresponda.
3. Copia el código HTML de esta página al template de AppComponent.
4. Copia las imágenes de la carpeta img a la carpeta assets del proyecto angular.
5. Programa tu aplicación para que haga lo siguiente:
 1. Al iniciarse, el semáforo de vehículos debe estar en verde y el de peatones en rojo. Los textos deben corresponderse en todo momento con los colores correspondientes.
 2. Al hacer clic en el botón "Iniciar semáforo", tienen que empezar los ciclos de luces, cambiando a cada segundo. El texto del botón cambiará a "Pausar semáforo".
 3. Al hacer clic en "Pausar semáforo", los ciclos de luces deben detenerse hasta que se pulse el botón de nuevo. El texto del botón cambiará a "Iniciar semáforo".

Para ejecutar un código cada x milisegundos:

```javascript
let intervalo = setInterval(function() {
    // Nuestro código
}, x);
```

Para detenerlo:

```javascript
clearInterval(intervalo);
```

